<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/utils/initTwig.php';
error_reporting(E_ALL ^ E_DEPRECATED);

use JosepBlanch\Refactoring\Infrastructure\UserController as UserController;
use \RuntimeException as Exceptions;

if ($_POST) {

    try {

        $userController = new UserController();
        $userController->checkRegisteredUserAction($_POST['email'], $_POST['password']);
        echo $twig->render ('messageReturn.twig',
                array ('message' => 'Access granted.', 'url' => '', 'backMessage' => ''));
    }
    catch (Exceptions $exception) {
        echo $twig->render('messageReturn.twig',
                array('message' => $exception->getMessage(), 'url' => 'login.php', 'backMessage' => 'Back.'));
    }
}
else {
    echo $twig->render('loginTemplate.twig');
}

