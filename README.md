# Kata Password Refactoring
* [Fuente del código original](http://www.codeofaninja.com/2013/03/php-hash-password.html)
* Fecha de publicación del código: 31 de Marzo de 2013 (eran otros tiempos)

# Introducción
Se ha refactorizado el código original y se ha añadido la funcionalidad de registrarse/logearse vía consola además de la ya existente mediante interfaz web.

# Cómo empezar
1. Clonar este repositorio.
2. Añadir las dependencias de composer con `composer update`.
3. Configurar el acceso a la base de datos.

# Registro/Login vía Web
* Para registrar un usuario nuevo: abrir en el navegador el archivo `register.php`.
* Para logear un usuario nuevo: abrir en el navegador el archivo `login.php`.


# Registro/Login vía Consola
* Para registrar un usuario nuevo por consola: `php consoleRegister.php`.
* Para logear un usuario existente por consola: `php consoleRLogin.php`.

