<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/utils/initTwig.php';
error_reporting(E_ALL ^ E_DEPRECATED);

use JosepBlanch\Refactoring\Infrastructure\UserController as UserController;
use \RuntimeException as Exceptions;

if ($_POST) {

    try {
        $userController = new UserController();
        $userController->registerUserAction($_POST['email'], $_POST['password']);
        echo $twig->render('messageReturn.twig', array('message' => 'Successful registration.', 'url' => '', 'backMessage' => ''));
    }
    catch (Exceptions $exception) {
        echo $twig->render('messageReturn.twig', array('message' => $exception->getMessage(), 'url' => 'register.php', 'backMessage' => 'Please try again'));
    }
}
else {
    echo $twig->render('registerTemplate.twig');
}
