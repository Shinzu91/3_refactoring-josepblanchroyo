<?php

$twigLoader     = new \Twig_Loader_Filesystem(__DIR__ . '/../templates');
$twig       = new \Twig_Environment( $twigLoader, ['debug' => true] );