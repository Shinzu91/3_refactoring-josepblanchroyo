<?php
require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL ^ E_DEPRECATED);

use JosepBlanch\Refactoring\Infrastructure\UserController as UserController;
use \RuntimeException as Exceptions;

echo "------GanianesCorp Login User Utility------\n";

echo "Insert Email: ";
$standardIn = fopen('php://stdin', 'r');
$line = fgets($standardIn);
$email = trim($line);

echo "Insert Password: ";
$standardIn = fopen('php://stdin', 'r');
$line = fgets($standardIn);
$password = trim($line);

try{
    $userController = new UserController();
    $userController->checkRegisteredUserAction($email, $password);
    echo 'Access granted.';
}

catch (Exceptions $exception){
    echo 'Error: '.$exception->getMessage();
}