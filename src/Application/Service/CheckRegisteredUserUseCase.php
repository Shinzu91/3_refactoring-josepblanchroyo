<?php

namespace JosepBlanch\Refactoring\Application\Service;

use JosepBlanch\Refactoring\Infrastructure\Repository\DatabaseUserReaderRepository;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserAccessDeniedException;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserExceptions;
use JosepBlanch\Refactoring\Domain\Model\PasswordHasher;
use JosepBlanch\Refactoring\Domain\Model\User;

final class CheckRegisteredUserUseCase
{
    private $passwordHasher;
    private $database;

    public function __construct (PasswordHasher $passwordHasher, DatabaseUserReaderRepository $database)
    {
        $this->passwordHasher = $passwordHasher;
        $this->database = $database;
    }

    public function __invoke(string $email, string $password)
    {
        try {
            $user = new User($email, $password);
            $existingUser = $this->database->getExistingUser($user);
            $this->passwordHasher->checkPassword($password, $existingUser->password());
        }
        catch(UserExceptions $exception){
            throw new UserAccessDeniedException;
        }
    }
}