<?php

namespace JosepBlanch\Refactoring\Application\Service;

use JosepBlanch\Refactoring\Infrastructure\Repository\DatabaseUserRegisterRepository;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserExceptions;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserUnableToRegisterException;
use JosepBlanch\Refactoring\Domain\Model\PasswordHasher;
use JosepBlanch\Refactoring\Domain\Model\User;

final class RegisterUserUseCase
{
    private $passwordHasher;
    private $database;

    public function __construct (PasswordHasher $passwordHasher, DatabaseUserRegisterRepository $database)
    {
        $this->passwordHasher = $passwordHasher;
        $this->database = $database;
    }

    public function __invoke(string $email, string $password)
    {
        try{
            $sanitizedEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
            if(!$sanitizedEmail) throw new UserUnableToRegisterException;
            $hashedPassword = $this->passwordHasher->hashPassword($password);
            $user = new User($sanitizedEmail, $hashedPassword);
            $this->database->registerUser($user);
        }
        catch (UserExceptions $exception){
            throw new UserUnableToRegisterException;
        }
    }
}