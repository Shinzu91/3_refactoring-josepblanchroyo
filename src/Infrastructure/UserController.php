<?php

namespace JosepBlanch\Refactoring\Infrastructure;

use JosepBlanch\Refactoring\Infrastructure\Repository\MySql\MySQLDatabaseUserReaderRepository;
use JosepBlanch\Refactoring\Infrastructure\Repository\MySql\MySQLDatabaseUserRegisterRepository;
use JosepBlanch\Refactoring\Application\Service\CheckRegisteredUserUseCase;
use JosepBlanch\Refactoring\Application\Service\RegisterUserUseCase;
use JosepBlanch\Refactoring\Domain\Model\PasswordHasher;

class UserController
{
    public function registerUserAction (string $email, string $password)
    {
        $database = new MySQLDatabaseUserRegisterRepository();
        $passwordHasher = new PasswordHasher();
        $registerUserUseCase = new RegisterUserUseCase($passwordHasher, $database);
        $registerUserUseCase($email, $password);
    }

    public function checkRegisteredUserAction(string $email, string $password)
    {
        $database = new MySQLDatabaseUserReaderRepository();
        $passwordHasher = new PasswordHasher();
        $checkRegisteredUserUseCase = new CheckRegisteredUserUseCase($passwordHasher, $database);
        $checkRegisteredUserUseCase($email, $password);
    }
}