<?php

namespace JosepBlanch\Refactoring\Infrastructure\Repository;

use JosepBlanch\Refactoring\Domain\Model\User;


interface DatabaseUserRegisterRepository
{

    public function registerUser(User $user);

}