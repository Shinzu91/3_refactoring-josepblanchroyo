<?php

namespace JosepBlanch\Refactoring\Infrastructure\Repository;

use JosepBlanch\Refactoring\Domain\Model\User;


interface DatabaseUserReaderRepository
{

    public function getExistingUser(User $user);

}