<?php

namespace JosepBlanch\Refactoring\Infrastructure\Repository\MySql;

use JosepBlanch\Refactoring\Infrastructure\Repository\DatabaseUserRegisterRepository;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserExceptions;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserUnableToRegisterException;
use JosepBlanch\Refactoring\Domain\Model\User;

class MySQLDatabaseUserRegisterRepository implements DatabaseUserRegisterRepository
{
    private $databaseConnection;

    public function __construct ()
    {
        $host     = "localhost";
        $db_name  = "coan_secure";
        $username = "root";
        $password = "";

        try {
            $this->databaseConnection = new \PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
        }
        catch (\PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public function registerUser(User $user)
    {
        try{
            $dbStatement = "INSERT INTO users SET email = ?, password = ?";
            $statement = $this->databaseConnection->prepare($dbStatement);
            $email = $user->email();
            $password = $user->password();
            $statement->bindParam(1, $email);
            $statement->bindParam(2, $password);
            $statement->execute();
        }
        catch( UserExceptions $e ){
            throw new UserUnableToRegisterException();
        }

    }

}