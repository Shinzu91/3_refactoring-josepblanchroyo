<?php

namespace JosepBlanch\Refactoring\Infrastructure\Repository\MySql;

use JosepBlanch\Refactoring\Infrastructure\Repository\DatabaseUserReaderRepository;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserAccessDeniedException;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserExceptions;
use JosepBlanch\Refactoring\Domain\Model\User;

class MySQLDatabaseUserReaderRepository implements DatabaseUserReaderRepository
{
    private $databaseConnection;

    public function __construct ()
    {
        $host     = "localhost";
        $db_name  = "coan_secure";
        $username = "root";
        $password = "";

        try {
            $this->databaseConnection = new \PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
        }
        catch (\PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public function getExistingUser (User $user): User
    {
        try {
            $query = "select email, password from users where email = ? limit 0,1";
            $statement  = $this->databaseConnection->prepare($query);
            $email = $user->email();
            $statement->bindParam(1, $email);
            $statement->execute();
            $rowsCounted = $statement->rowCount();

            if ($rowsCounted == 1) {
                $resultRow      = $statement->fetch (\PDO::FETCH_ASSOC);
                $user = new User($resultRow['email'], $resultRow['password']);
                return $user;
            }
            throw new UserAccessDeniedException;
        }

        catch ( UserExceptions $e ){
            throw new UserAccessDeniedException;
        }

    }

}