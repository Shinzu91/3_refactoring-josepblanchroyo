<?php

namespace JosepBlanch\Refactoring\Domain\Model;

class User
{
    private $email;
    private $password;

    public function __construct (string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function password() : string
    {
        return $this->password;
    }

    public function email() : string
    {
        return $this->email;
    }
}