<?php

namespace JosepBlanch\Refactoring\Domain\Model\Exceptions;

class UserAccessDeniedException extends UserExceptions
{
    protected $message = "Access denied.";
}