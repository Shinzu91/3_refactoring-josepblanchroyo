<?php

namespace JosepBlanch\Refactoring\Domain\Model\Exceptions;

class UserUnableToRegisterException extends UserExceptions
{
    protected $message = "Unable to register.";
}