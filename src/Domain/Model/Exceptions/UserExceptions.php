<?php

namespace JosepBlanch\Refactoring\Domain\Model\Exceptions;

abstract class UserExceptions extends \RuntimeException
{
    protected $message = "Error found";
}