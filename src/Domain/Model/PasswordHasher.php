<?php

namespace JosepBlanch\Refactoring\Domain\Model;

use JosepBlanch\PasswordHash\PasswordHashLibrary as PasswordHashLibrary;
use JosepBlanch\Refactoring\Domain\Model\Exceptions\UserAccessDeniedException;

class PasswordHasher
{
    CONST SALT = "ilovecodeofaninjabymikedalisay";
    CONST BASE_2_LOGARITHM_ITERATION = 8;
    CONST OLDER_SYSTEM_PORTABLE = false;

    private $hasher;

    public function __construct ()
    {
        $this->hasher = new PasswordHashLibrary();
        $this->hasher->PasswordHash(self::BASE_2_LOGARITHM_ITERATION,self::OLDER_SYSTEM_PORTABLE);
    }

    public function hashPassword(string $password) : string
    {
        
        $saltedPassword = $this->saltPassword($password);
        $passwordHashed = $this->hasher->HashPassword($saltedPassword);
        return $passwordHashed;
    }

    public function checkPassword(string $password, string $storedPassword)
    {

        $saltedPassword = $this->saltPassword($password);
        $passwordChecked = $this->hasher->CheckPassword($saltedPassword, $storedPassword);
        if(!$passwordChecked)
        {
            throw new UserAccessDeniedException;
        }


    }

    private function saltPassword(string $password) : string
    {
        $saltedPassword = (self::SALT . $password);
        return $saltedPassword;
    }

}